#!/bin/sh

if [ -z $WAYLAND_DISPLAY ]; then
	(>&2 echo Wayland is not running)
	exit 1
fi

## Put the path to your own screenshot folder
## to variable SWAYSHOT_SCREENSHOTS here: ~/.config/swayshot.sh
if [ -f ~/.config/swayshot.sh ]; then
	source ~/.config/swayshot.sh
fi

if [ -z $SWAYSHOT_SCREENSHOTS ]; then	
	SWAYSHOT_SCREENSHOTS=$(xdg-user-dir PICTURES)
fi

SCREENSHOT_TIMESTAMP=$(date "+${SWAYSHOT_DATEFMT:-%F_%H-%M-%S_%N}")

SCREENSHOT_FULLNAME="$SWAYSHOT_SCREENSHOTS"/screenshot_${SCREENSHOT_TIMESTAMP}.png

COPY=0

printHelp() {
				echo "Usage: swayshot [options] [display|window|region]

General:
-h, --help					Show this help message
-t, --temporary					Don't save the screenshot
-c, --copy					Copy the screenshot to clipboard, not it's path"
}

printError() {
	echo $@ > /dev/stderr
	printHelp
	exit
}

filter='
# returns the focused node by recursively traversing the node tree
def find_focused_node:
if .focused then . else (if .nodes then (.nodes | .[] | find_focused_node) else empty end) end;
	# returns a string in the format that grim expects
	def format_rect:
	"\(.rect.x),\(.rect.y) \(.rect.width)x\(.rect.height)";
	find_focused_node | format_rect
	'

	args=`getopt htc $*`
	set -- $args
	while :
	do
		case "$1" in
			-h | --help)
				printHelp
				exit
				;;
			-t |--temporary)
				SCREENSHOT_FULLNAME="$(mktemp)"
				shift
				;;
			-c |--copy)
				COPY=1	
				shift
				;;
			--)
				shift
				break
				;;
			*)
				printError "$key - unknown option"
				shift
				;;
		esac
	done

	case "$1" in
		""|display)
			grim -o "$(swaymsg --type get_outputs --raw | jq --raw-output '.[] | select(.focused) | .name')" "$SCREENSHOT_FULLNAME"
			;;
		region)
			grim -g "$(slurp -b '#AFAFAFAF' -c '#FF3F3FAF' -s '#00000000' -w 3 -d)" "$SCREENSHOT_FULLNAME"
			;;		
		window)
			grim -g "$(swaymsg --type get_tree --raw | jq --raw-output "${filter}")" "$SCREENSHOT_FULLNAME"
			;;
		*)
			printError "$1 - unknown action"
			;;
	esac


	if [ -x "$(command -v wl-copy)" ]; then
		if [ $COPY -eq 0 ]; then
			echo -n "$SCREENSHOT_FULLNAME" | wl-copy
		elif [ $COPY -eq 1 ]; then
			wl-copy < "$SCREENSHOT_FULLNAME"
		fi
	elif [ -x "$(command -v xsel)" ]; then
		echo -n "$SCREENSHOT_FULLNAME" | xsel --clipboard
	elif [ -x "$(command -v xclip)" ]; then
		echo -n "$SCREENSHOT_FULLNAME" | xclip -selection clipboard
	else
		echo -n "$SCREENSHOT_FULLNAME"
	fi
