# Contributing to swayshot

1. Fork it!
2. Commit your changes: `git commit -m 'Add some feature'`
3. Push to the GitLab
4. Submit a pull request :D